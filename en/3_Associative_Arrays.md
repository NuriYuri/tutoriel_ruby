# Associative Arrays

The associative array type in Ruby is called Hash. It's useful for several things:
* Easily representing data without needing to define a class for it.
* Storing parameters for a method
* Storing key-value pairs (which can eliminate the need for long chains of conditionals)

### Creating a Hash

In the [Variables and Data Types](1_Variables_and_Data_Types.md) section, we saw how to define a hash using literals. There is a `new` method that lets you define a default value, which you can be very helpful.

By default, when you key that doesn't have a value paired with it, the value returned is null. It's possible to change this upon creating the hash:

```ruby
Hash.new(default_value)
```

Just like for arrays, the default value is not copied, so it's preferable to use a block when creating a hash.
```ruby
Hash.new { |hash, key| next(value) }
```

The `hash` parameter is the hash itself and the `key` parameter is the key that's being accessed. This lets you run a little bit of code every time a key is accessed in a hash.

Important note: If you create a hash using a block, it won't be able to be serialized. The serialization utilities like JSON, YAML, and Marshal don't know how to serialize code.

### Reading the Value Associated With a Key

In associative arrays, keys are like the index in an ordered array. This is why we access hashes via a key using the same operator: `[]`.

Example:
```ruby
hash = { name: 'Yuri', age: 25 }
hash[:name] # "Yuri"
hash[:age] # 25
```

### Modifying the Value Associated With a Key

Likewise, modifying values can be done with the `[]=` operator.

Example:
```ruby
hash = { name: 'Yuri', age: 24 }
hash[:age] = 25 
# hash = { :name => "Yuri", :age => 25 }
```

### Checking For a Key

To find if a hash contains a key, use the `key?` method. The `has_key?` method is an alias.

Example:
```ruby
hash = { name: 'Yuri', age: 25 }
hash.key?(:name) # true
hash.key?(:size) # false
```

### Checking For a Value

Similarly, we can check for a value in the hash using `value?`. Its alias is `has_value?`.

Example:
```ruby
hash = { name: 'Yuri', age: 25 }
hash.value?('Yuri') # true
hash.value?(42) # false
```

### Deleting a Key

To delete a key from a hash, call the `delete` method.

Example:
```ruby
hash = { name: 'Yuri', age: 25 }
hash.delete(:name)
# hash = { :age => 25 }
```

### Determining the Number of Keys

To determine the number of keys in a hash, call `size` or `length`.

Example
```ruby
hash = { one: 1, two: 2, four: 4 }
hash.size # 3
```

### Extracting Certain Key-Value Pairs

The `slice` method will pull what you ask it to out of the hash.

Example:
```ruby
hash = { name: 'Yuri', age: 25, size: 950, location: 'mine' }
hash.slice(:name, :age) # { :name => 'Yuri', :age => 25 }
```

### Merging Two Hashes

Sometimes you want to merge two hashes (if they have related data, for example). To do this, you can use `merge` (creates a new hash) or `merge!` (modifies the hash in place).

Note: If two identical keys appear in a hash, the hash passed as a parameter takes priority.

Example:
```ruby
hash1 = { name: 'Yuri' }
hash2 = { age: 25 }
hash1.merge!(hash2)
# hash1 = { :name => "Yuri", :age => 25 }
```

## Iteration

Like with arrays, sometimes it's useful to be able to iterate through a hash. There are three methods for this, equivalent to the ones for arrays:
* `each_value`: runs through all the values in the hash.
* `each`: runs through all the key-value pairs in the hash.
* `each_key`: runs through all the keys in the hash.

Examples:
```ruby
hash = { name: 'Yuri', age: 25 }
hash.each_value { |value| p value }
# output:
# "Yuri"
# 25
hash.each { |key, value| puts "hash[#{key.inspect}] = #{value.inspect}" }
# output:
# hash[:name] = "Yuri"
# hash[:age] = 25
hash.each_key { |key| p key }
# output:
# :name
# :age
```

Just like with arrays, there are methods that will let you modify a hash in various ways, but I'll let you read through the documentation to find out more about them.