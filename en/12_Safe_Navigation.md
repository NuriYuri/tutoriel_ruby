# Safe Navigation

Starting with Ruby 2.3.0, there's been an operator that simplifies "navigation" operations. This operator is the `&.` .

## What do we mean by "navigation"?

Navigation is what we call the process of going into an object's properties or sub-objects to get information. In certain instances a variable that should contain an object will be uninitialized (will have the value `nil`). To avoid errors, it's common practice to do this:
```ruby
if variable && variable.property
  # do something with variable.property
end
```
Or we can write this in an optimized way:
```ruby
if (property = variable && variable.property)
  # do something with property
end
```

This lets us safely access the object's properties or call its methods.

## Safe Navigation

Safe navigation is a principle similar to the scenario above, except the we only call a method if the object is not `nil`.

The examples above can be written like this with the `&.` operator:
```ruby
if variable&.property
  # do something with variable.property
end
```
Or even:
```ruby
if (property = variable&.property)
  # Do something with property
end
```

The advantage of this operator's behavior over using `&&` is that the value `false` can have properties accessed or methods called.
Example:
```ruby
str = variable&.to_s
```
If variable is `nil`, str will contain `nil`. However, if the variable is `false` str will contain `"false"`. The `to_s` method of FalseClass will be called.

## Sources

This page was written from some research on $SAFE levels I was doing that had nothing to do with the content of this page, but through the magic of search engines I randomly wound up on an article talking about this:
[The Safe Navigation Operator (&.) in Ruby](http://mitrev.net/ruby/2015/11/13/the-operator-in-ruby/) by Georgi Mitrev.

I strongly recommend you read the original source, it's more detailed than this page :)