# Input/Output

In Ruby, input and output operations are done with `IO`. There are classic `IO` patterns such as `STDIN` (standard in) `STDOUT` (standard out), and pipes.
Files are done with `File` and sockets are handled with `TCPSocket`.

There is a sizeable collection of I/O methods:
* `<<`: Insert the element to the left into `IO`
* `binmode`: Switches `IO` into binary mode.
* `binmode?`: Checks if `IO` is in binary mode.
* `close`: Closes `IO`.
* `close_read`: Stops reading from `IO`.
* `close_write`: Stops writing to `IO`.
* `closed?`: Checks if `IO` is closed.
* `each` / `each_line`: Reads all the lines from `IO` and passes each into a block.
* `each_byte`: Reads each byte from `IO` and passes each into a block.
* `each_char`: Reads each character from `IO` and passes each into the block as a string.
* `each_codepoint`: Reads each character from `IO` and passes each into a block in raw form.
* `eof?`: Checks if `IO` has reached the end of its content.
* `external_encoding`: Checks if the encoding of `IO` has been specified. (Returns `nil` if not.)
* `fileno`: Returns a file descriptor.
* `flush`: Sends the `IO` buffer to be handled by the operating system.
* `getbyte`: Reads a byte from `IO` in raw form.
* `getc`: Reads a character from `IO` as a string.
* `gets`: Reads a string from `IO`.
* `internal_encoding`: Returns Ruby's string encoding setting, if it's been set (Returns `nil` if not).
* `lineno`: Returns the line number (counting up from 0) that the `IO` read operation is currently on.
* `pos`: Gets the offset (in bytes) the `IO` is at, relative to its contents.
* `print`: Takes an object as a parameter and writes it to `IO`.
* `printf`: Writes to `IO`, used in the same way we use `format`.
* `putc`: Writes a character to `IO`.
* `puts`: Writes a line to `IO`.
* `read`: Read `IO`.
* `read_nonblock`: Reads from `IO` without blocking the program if reading is unsuccessful.
* `readbyte`: Reads a byte from `IO`.
* `readchar`: Reads a character from `IO`.
* `readline`: Reads a line from `IO`.
* `readlines`: Reads all the lines from `IO`.
* `readpartial`: Takes a number of bytes and reads that many or fewer bytes.
* `rewind`: Resets `IO` cursor to the beginning.
* `seek`: Places the `IO` cursor at the specified position.
* `set_encoding`: Changes the encoding of `IO`.
* `stat`: Returns a `File::Stat` that describes `IO`.
* `sync`: Returns whether `IO` is automatically `flush`'d or not when content is added.
* `tty?`: Get whether `IO` is a terminal or not.
* `write`: Writes to `IO`.
* `write_nonblock`: Writes to `IO` without blocking `IO` needs synchronization time while writing.

## Files

Files are kinds of `IO` that contain data on disk. They can be opened by providing their name. Once open, you can perform various operations on them.

### Opening a File

To open a file, use the `open` method. It will return the opened file.

You can use a block to execute code and then close the file once the block's code is done executing.

Examples:
```ruby
file = open(name, mode)
# Do thing with file
file.close

open(name, mode) do |file|
  # Do thing with file
end
```

You will be able to do everything available in the IO class, so long as it lines up with the `mode` used to open the file.

The `mode` is a string and can be any one of the following:
* `r` read only, starts at the beginning of the file.
* `r+'` read/write, starts at the beginning of the file (does not erase file contents).
* `w` write only, and erase existing file contents.
* `w+` read/write, and erase existing file contents.
* `a` write, appending to the end of the file. (Create the file if it doesn't exist.)
* `a+` write, appending to the end of the file, and allow reading. (Create the file if it doesn't exist.)

The mode letter can be accompanied by:
* `b` to open in binary mode.
* `t` to open in text mode.

After the mode string, you can specify a file encoding by adding `:<encoding_name>`, where `<encoding_name>` is the file encoding.

To automatically delete the BOM for UTF files, add `BOM|` before the encoding name.

Example:
```ruby
File.open('test.txt', 'r:BOM|UTF-8') do |f| 
  # do some reading of f
end
```

The encoding for the internal representation can also be specified with an additional `:<encoding_name>` added onto the encoding string.

## Partial and Full File Reads

If you don't intend to perform several read operations on a file, you can use a function to read the entire file or a fragment without creating an `IO`:
* `File.read`: Similar to `'r'` mode but usually returns a string in ASCII-8BIT format.
* `File.binread`: Similar to `'rb'` mode.

The first parameter of these functions is the file name. The other optional parameters are the read size and offset.

Examples: 
```ruby
File.read('test.txt') # Reads all of test.txt and returns its contents in a string.
File.read('test.txt', 50) # Reads the first 50 characters of test.txt and returns them as a string.
File.read('test.txt', 50, 2) # Reads 50 starting from the 3rd character in test.txt and returns them as a string.
```

## Partial or Full File Writes

You can also use the following methods to create an `IO` for writing to a file:
* `File.write`: Write equivalent to the `'w'` mode.
* `File.binwrite` : Write equivalent to the `'wb'` mode.

Their parameters are:
* The file name
* The content to write
* The content offset (optional - if given, does not replace the file contents with the content written).

Example:
```ruby
File.write('test.txt', 'Hello World!')
# test.txt will contain : Hello World!
File.write('test.txt', 'Home!', 6)
# test.txt will contain: Hello Home!!
```

## Copying a File

You can copy a file with this function: `File.copy_stream`.

Its parameters are:
* The name of the source file.
* The name of the destination file.
* The copy size (optional)
* The source file offset (optional)

Example:
```ruby
# Copy all of test.txt into test-copy.txt:
File.copy_stream('test.txt', 'test-copy.txt')
# Copy a part of test.txt into test-truncated.txt:
File.copy_stream('test.txt', 'test-truncated.txt', 512)
# Copier a section of test.text:
File.copy_stream('test.txt', 'test-section.txt', 512, 1024)
```

## Renaming a File

You can easily rename a file by using `File.rename`.

The first parameter is the old name of the file and the second is the file's new name.

## Reading a File's Size

To read a file's size, use `File.size` with a file name as a parameter. When you open a file, there is a `size` method that takes no parameters and returns the size of the opened file.

## Opening a Pipe

Pipes, for anyone curious, are I/O channels between two programs, generally a parent program and a child program. In Ruby, open a pipe with `IO.popen`.

Example: 
```ruby
IO.popen('date', 'r+') { |date| puts "Date : #{date.readline}" }
```

In this example, I open a pipe to the "date" program. Generally when I do this I'm only interested on the date displayed on the first line of the output.