# Variables and Data Types
Like in most programming languages, in Ruby we assign and use variables that contain data.

In Ruby, there are several types of variables. There are also several types of data. Let's start with the variable types, then we'll move on to the data types.

## Variables
A variable is a part of a program that associates a name (an "identifier") and a value. The value part of a variable can change over time, which is why they're called "variables". Depending on the context, the same name can also be associated with a different value. This depends on what's called the "scope" of the variable.

### Variable Scope
In Ruby, the scope of a variable is explicit based on its name: the first few characters of a name show its scope.

These are the different types of scope :
* **Global Variables**

    These names start with a `$` and are followed by any other non-reserved characters.

    These variables are accessible everywhere and can be modified everywhere. Because of their scope, accessing one of these variable names when it hasn't been assigned a value returns `nil` (the empty object).

* **Instance Variables**

    Instance variable names are like global variables, but instead they start with a `@`.

    These variables are only accessible from within the context of an object, which means the value associated with this variable can change from object to object. These variables are used to store information specific to each object.

* **Class Variables**

    These variables are for storing information specific to a class of object. They start with two `@` symbols and should not be confused with instance variables since, unlike instance variables, it is not possible to access an uninitialized class variable.

    The scope of a class variable is kind of narrow. In the context of a class that has defined this variable inside itself and inside its child classes, the variable is accessible. However, in parent classes this variable is inaccessible.

    This can cause a lot of frustration when writing methods that access these variables, which is why you should avoid these types of variables if at all possible.

* **Constants**

    Even though this is called a "constant", it's still a type of variable. These variables always start with a capital letter. Since Ruby 2.6, this can also be a non-Latin character (a capital Greek letter, for example).

    The scope of these variables is a bit similar to the scope of class variables, the difference being that in different child classes these variables can be associated with a different value.

    I'd like to also call your attention to the way these variables are accessed: when you write the name of a constant in your code (in a method), the constant that will be used does not come from the object context you're in, rather it comes from the context where the method was defined. This means if a method was inherited from a parent class and the constant has a different value than in the current class, the value the method reads comes from the constant in the parent class.

    That being said, there are ways of accessing constants that allow you to be 100% sure that you're accessing it in the right context.

* **Local Variables**

    Local variables have a name that starts with any character not mentioned in the variable types above. Note that the `_` and non-breaking space characters are not valid characters and so they cannot be used in local variable names. (See [Not a funny script](../scripts/not_a_funny_script.rb))

    The scope of these variables is local, as the name indicates. If the context changes, the value associated to these variables changes, assuming a variable with that name has been defined in the new context.

    Be aware that methods have the same naming rules as local variables, which can sometimes be confusing.

## Data Types
In Ruby there are several data types, each with specific uses.

### Numbers
Numbers are data of type Numeric. In this category there are whole numbers (Integer), floating-point numbers (Float), rational numbers (Rational), and complex numbers (Complex).

There are several ways to define these numbers or convert objects into these numbers.

#### Whole Numbers

Whole numbers can be written a few different ways:
* If it's a binary whole number, it's written as: `0b<number>` where `<number>` is a sequence of binary digits.
* If it's an octal whole number, it's written as: `0<number>` where `<number>` is a sequence of octal digits.
* If it's a hexadecimal whole number, it's written as: `0x<number>` where `<number>` is a sequence of hexadecimal digits.
* Otherwise, it's written as series of decimal digits.

Whole numbers can be separated by an underscore ( `_` ) to separate numbers into thousands places or chunks of digits.

Examples:
```ruby
0xDEADBEEF # 3735928559
0b1011_0010 # 178
0755 # 493 or rwxrw-rw-
1_000_000
```

#### Floating-Point Numbers

Floating-point numbers are written with a decimal to separate the whole number from the floating point portion, or with an `e` character to define the exponent of the number as in scientific notation. The two styles can be combined.

Examples:
```ruby
1e2 # 100.0
0.5
0.314e1 # 3.14
```

#### Complex Numbers

Complex numbers are expressed the same way as in mathematics. A complex number has an `i` after the number.

Examples:
```ruby
5i # (0+5i)
3 + 2i # (3+2i)
1 + 0.33i # (1+0.33i)
0.14e3i # (0+140.0i)
```

#### Rational Numbers

Rational numbers are expressed with an `r` character after the number. When you're defining a complex number, the `r` comes before the `i`.

Keep in mind it's not possible to use scientific notation (`e`) with rational notation (`r`).

Examples:
```ruby
5r # (5/1)
0.33r # (33/100)
0.3ri # (0+(3/10)*i)
```

#### Numeric Conversion
We've seen how to write numbers, now it'll be useful if we can convert a given numeric type into another numeric type.

* Convert to whole number: number.to_i
* Convert to floating-point number: number.to_f
* Convert to complex number: number.to_c
* Convert to rational number : number.to_r

Examples:
```ruby
3.14.to_i # 3
10.to_f # 10.0
1.to_c # (1+0i)
5.to_r # (5/1)
```

Important note: Complex numbers can't be converted into rational numbers (I don't know why) but rational numbers can be converted into complex numbers.

#### Numeric Operations

Several kinds of operations can be done on numbers: 
* Addition: `a + b`
* Subtraction: `a - b`
* Negation: `-a`
* Multiplication: `a * b`
* Power operations: `a**n`
* Division: `a / b`
* Remainder after division (modulo): `a % b`

    Warning: Complex numbers don't support the modulo operation.

For whole numbers there are some logical operations:
* Bitwise logical AND: `a & b`
* Bitwise logical OR: `a | b`
* Bitwise logical exclusive-OR (XOR): `a ^ b`
* Bitwise negation: `~a`

### Character Strings

Character strings allow us to describe text. Their main use is to deal with files and display messages to the user. In performance critical portions of a program, strings should not be used to index elements because dealing with strings is slow and unwieldly.

There are several ways to define character strings:

* Simple Strings: `'my string'`

    This string syntax allows us to define raw, uninterpreted strings (meaning no escape characters). The only escape character accepted in these strings is `\'` since the single quote is the character used to delimit the string.

* Simple Percent Strings: `%q(my string)`

    These strings are equivalent to simple strings, except they allow you to use single and double quotes at the same time without escaping them.

* Advanced Strings: `"My string"`

    This type of string allows interpolation and escape characters. Use only if you need this functionality.

* Advanced Percent Strings: `%Q(my string)`

    This type of string functions exactly like simple percent strings, except they support interpolation and escape characters.

Examples: 

```ruby
'Simple string #{0}\n' # "Simple string \#{0}\\n"
%q("this one's also simple #{0}\n") # "\"this one's also simple \#{0}\\n\""
"This one's advanced #{0}\n" # "This one's advanced 0\n"
%Q("This one's also advanced #{0}\n") # "\"This one's also advanced 0\n\""
```

### Symbols

Symbols are a magic tool Ruby uses to index elements or find other elements. Symbols are immutable, meaning their object id is always the same (unlike strings). This is why Ruby uses symbols to identify methods, constants, instance variables, and lots of other things.

To write a symbol, start with the `:` character.
Depending on the variable type, you may need to follow that by another specific character.

Examples:
```ruby
:method_name # Symbol for a theoretical method name
:@instance_variable # Symbol for a theoretical instance variable name
:CONSTANT_NAME # Symbol for a theoretical constant name
:"name with spaces" # Symbol that should be used sparingly, only if the name contains characters that force the symbol name to be delimited.
```

Note: Strings can be converted to numbers.

To convert anything into a string, use the method `to_s`.

### Ordered Arrays

Ordered arrays allow us to store objects in a linear space, indexed by numbers (starting with 0). These sequences are written with square brackets and are separated by commas. They also contain tons of different methods that will help us manipulate them, as we'll see later.

Examples:

```ruby
[] # Empty array
[0, '1', :two] # Array containing 3 elements of arbitrary type
[[1, 2], [3, 4]] # Array containing arrays
```

### Associative Arrays

Associative arrays are arrays that use an object as a key and associate it with a value object. Most of the time these arrays use symbols to index their values. However, there are certain **bad programmers** who intentionally use strings to index their associative arrays.

Using objects other than symbols to index values in an associative array has to be justified by some need (conversion tables, limitations imposed by some standard like JSON, etc...).

The syntax for associative arrays uses curly brackets. If you use symbols, put the `:` at the end of the name instead of at the beginning to make the boundary between the key and the value clear. If you use other objects (numbers, for example), use `=>` to delimit the key and the value.

Example:

```ruby
{} # Empty associative array
{ name: "Yuri", age: 25, nationality: :French } # Array with data
{ 1 => "January", 2 => "February" } # Array for converting from a number to a month
```

### Booleans

Unlike other languages, Ruby has booleans which **are not** equivalent to numbers. The booleans are:
* `true` when something is true.
* `false` when something is false.

In Ruby, `0` is not equivalent to `false`, so don't get them confused.

### Objects

Objects are all the data types which are descendants from Object. These are instances of more complex ideas (Point, Folder, StudentList, etc...).

We'll see later how to describe these ideas as classes, and how to manipulate objects.

## Variable Assignment

In Ruby, the assignment operator is `=`. It can be combined with other operators to do operations on the variable.

Assignment examples:
```ruby
number = 5
# number = 5
number = number * 5
# number = 25
number += 5
# number = 30
```

That last operator `+=` is actually a shortcut for: `variable = variable + value`. You can use this shortcut with the following operators: `+`, `-`, `*`, `/`, `%`, `&`, `|`, `^`, `**`, `&&`, `||`.

## Variable Type Checking

To check the data type of a variable we have the `is_a?` method, which tells us if an object has the specified type. (This also accounts for inheritance, which we'll see later).

Example: 
```ruby
my_variable = 5
my_variable.is_a?(String) # false
my_variable.is_a?(Numeric) # true
my_variable.is_a?(Complex) # false
my_variable.is_a?(Integer) # true
```
Now we know my_variable contains a number, specifically a whole number.