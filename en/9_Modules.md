# Modules
In Ruby, modules are kind of like namespaces that are used for implementing what we call Mixins.

## Defining a Module

Defining a module is pretty easy. We use the keyword `module` and follow it by a name formatted as a constant, like we did with classes.

Example:
```ruby
module MyModule

end
```

## Module Contents

A module can contain:
* Classes (namespaces)
* Modules (namespaces)
* Constants (namespaces)
* Methods (Mixins)
* Module methods (Mixin + namespace)

The perfect example is the `Math` module. `Math::PI` is the pi constant. We can also access the cosine function using `Math.cos`. 

## Including a Module

If you want to access the cosine function in your class's code, you can use the `include` method within your class to include module functions inside it. Then you can avoid having to fully write out `Math.cos`.

Example:
```ruby
class MyClass
  include Math

  # Function using cos
  # @return [Float]
  def use_cos
    cos(PI / 2)
  end
end
```

As you can see, we also have access to module constants.

## Defining Module Methods

When you define methods in a module, there are two possible scenarios you can find yourself in:
* You want to only use these methods via Mixins, in which case you won't give any specific implementation of these methods.
* You want both to use these methods as Mixins and access them directly (like in the `Math.cos` example). To do this you use the `module_function` keyword just like the `private` and `public` keywords.

Example:
```ruby
# My module
module MyModule
  # Method that does something (only visible when included)
  def do_something
  end

  # Method that can be called like this: MyModule.method_name
  module_function def method_name
  end

  # All the following methods will be module_function
  module_function
  
  # Other methods that can be called like this: MyModule.other_method_name
  def other_method_name
  end
end
```

Note that the `module_function`s will be private once they're included, in contrast to normal methods which, without specifying, will default to public once included.

## Extend

In Ruby there is a function for including the methods of a module in a class, module, or object in a way similar to singleton methods. This is called `extend`.

The methods copied from a module with `extend` will have the same attributes as in their module of origin.

Example:
```ruby
# My class
class MyClass
  extend MyModule
end
```

We'll now be able to call `MyClass.do_something`, whereas the `method_name` and `other_method_name` methods are private. They won't be able to be called by MyClass's singleton methods.

## Accessing Constants

Since we use modules like namespaces, sometimes we run into a situation where constants have the same names in several modules:
* `A::MyClass`
* `B::MyClass`
* `MyClass`

If in module `B` you want to create a class that inherits from `MyClass` in global scope, you're in trouble because `MyClass` in the `B` module is referencing `B::MyClass`.
To deal with this, Ruby has a simple solution for us: You can think of the `::` like a `/` in UNIX systems. This basically means that if you want to access `MyClass` from global scope from within the `B` module, you just have to write `::MyClass`.

To access the `A` module's `MyClass` method from within the `B` module, you have two solutions:
* `A::MyClass`: If there's no A constant defined in the `B` module, Ruby will take a look all through the `Object` class, then if it finds it doesn't exist, it'll throw an error.
* `::A::MyClass`: Here, you're saying what you want explicitly. It's not necessarily more optimized but at least you're not running the risk of loading the wrong class.

Example:
```ruby
class MyClass
end
module A
  class MyClass
  end
end
module B
  class MyClass
  end

  p MyClass # Prints: B::MyClass
  p ::MyClass # Prints: MyClass
  p A::MyClass # Prints: A::MyClass
  p ::A::MyClass # Prints: A::MyClass
end
```

## Closing Thoughts
With this, I've given you all you need to create things in Ruby. The only thing left is to go over error handling and a few extra bits.