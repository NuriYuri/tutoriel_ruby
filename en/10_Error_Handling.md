# Error Handling

Ruby has an error handling system that will let you fairly easily debug your program and also control/skip execution of specific portions of code depending on certain conditions.

There are plenty of times when you could throw errors, but they're intended to be for critical scenarios only. For example, avoid raising errors for incorrect types when you could fix that yourself (when passed to functions, etc.)

## Throwing an Exception

To throw an exception, you use the `raise` function, which tells Ruby to stop currently executing code and to pass the designated exception through the exception pipeline.

There are three ways to call `raise` :
* `raise "My error message"`: When you don't have a specific error class, it'll raise a RuntimeError with "My error message" as its error message.
* `raise ErrorClass, "My error message"`: When you have a specific error class you want to use (ErrorClass in this case) it'll raise an ErrorClass with "My error message" as its message.
* `raise`: Use this when you've caught an exception (using `rescue`), and you want to rethrow the exception to be caught by another `rescue` further up the call stack.

## Catching an Exception

To catch an exception, use the `rescue` keyword followed by an expression describing the error type that you want to catch. You can put several `rescue`s in a row to handle different situations.

Generally, the `rescue` keyword is used in a `begin` --> `end` or a `def` --> `end`.

Example:
```ruby
def my_method(value)
  value * 5
rescue NoMethodError
  puts "The value parameter (#{value}) is incorrect, it does not support the `*` method"
  return 0 # We return a value to avoid causing further errors
rescue StandardError
  puts "The value parameter (#{value}) is doing something unexpected with the `*` method. You definitely have a critical problem with your program. Stopping..."
  raise
end

def *(v)
  raise "Bad thing"
end

my_method(5) # 25
my_method(:a) # 0
# Prints:
# The value parameter (a) is incorrect, it does not support the `*` method.
my_method(self)
# Prints:
# The value parameter (main) is doing something unexpected with the `*` method. You definitely have a critical problem with your program. Stopping...
# Traceback (most recent call last):
#        4: from E:/Ruby25/bin/irb.cmd:19:in `<main>'
#        3: from (irb):lineno
#        2: from (irb):lineno:in `my_method'
#        1: from (irb):lineno:in `*'
#RuntimeError (Bad thing)
```

## Re-executing Code after Correcting the Cause of an Exception

When you catch an exception and you're able to correct the problem (failing to create a file, for example), you'll want to get back to the normal code rather than killing the program or leaving the method. Use the Ruby keyword `retry` to do this. This keyword will allow you to re-execute code starting from `begin` or a `def` where a `rescue` was defined. Retry does not reinitialize the method context so any local variables will persist in the `rescue`. 

Example: 
```ruby
def read_collision_map(id_map)
  file_name = format('Data/Map%<id>03d.collision.rxdata', id: id_map)
  return load_file(file_name)
rescue Errno::ENOENT
  puts "The file #{file_name} does not exist..."
  regenerate_collisions(id_map)
  retry
end
```
I abstracted some of this out with helper methods because we haven't seen how to work with files yet. The `Errno::ENOENT` error is the error type that's usually raised when a file doesn't exist.

## Executing Code No Matter What

In methods you can use the keyword `ensure`, which will execute code no matter what problems occur before the method finishes.

This code gets executed in two cases:
* We hit the `return` or the method otherwise terminated normally.
* An exception was thrown and is done being handled.

Example:
```ruby
def my_method
  # buggy code
rescue StandardError
  puts 'An exception occurred'
  raise
ensure
  puts 'Cleaning things up'
end

my_method
# Prints:
# An exception occurred
# Cleaning things up
# <LogErrorRuby>
```

This can be very useful when you're writing online systems or things that require you to do some housekeeping tasks no matter what might go wrong (closing files, etc...)

## List of Error Types

The main type: `StandardError`, which will catch all the following errors :
* `ArgumentError`: Error with method or lambda parameters.
    * `UncaughtThrowError`: Error after you use `throw` with a tag missing a `catch`.
* `EncodingError`: Error when you modify strings or regular expressions with different encodings. If your data comes from a deserialized file (using Marshal) the encoding will be ASCII-8Bit, which is incompatible with everything in Ruby. Use `str.force_encoding('UTF-8')` to be sure your string will always be in UTF-8.
* `FiberError`: When you're using a `Fiber` dangerously (calling `resume` when the `Fiber` is dead, for example).
* `IOError`: Error when you are accessing a file that can't be accessed in the way you want (closed, opened in the wrong mode, etc...)
    * `EOFError`: When you've reached the end of a file and you try to keep reading.
* `IndexError`: When you are reading an array with `fetch` but you access an index outside the range of valid indexes.
    * `KeyError`: When you are reading a Hash using `fetch` but you use a key that hasn't been defined in that Hash.
    * `StopIteration`: When you reach the last iteration of an Enumerator (by calling `next`), or you can use to stop a `loop`.
*  `LocalJumpError`: This error  occurs when you use `yield` on something that's not a block, or you use `return` in a block that's not being executed inside a method.
* `NameError`: Error when you try to access something that hasn't been defined (Constant, method, local variable, etc...)
    * `NoMethodError`: Error when you explicitly call an object method when that object has no definition for the method called.
* `RangeError`: Error when native functions try to convert numbers into a format supported by C but those numbers are too big.
    * `FloatDomainError`: Error when you use `Float::INFINITY` or `Float::NAN` in certain types of operations.
* `RegexpError`: Error when your Regexp is incorrectly formatted.
* `RuntimeError`: This is a user error that contains helpful information in its message.
    * `FrozenError`: Error when you try to modify a frozen (unmodifiable) object.
* `SystemCallError` are all errors of type `Errno::`. A list of the `Errno` constants with descriptions is here: [ERRNO](http://man7.org/linux/man-pages/man3/errno.3.html)
* `ThreadError`: Error when an invalid operation is done to a Thread.
* `TypeError`: Error when a parameter type is invalid. (Often happens with native methods).
* `ZeroDivisionError`: This error occurs when you do division by 0 (division or modulo).


There are also several types of errors that are not caught by StandardError:
* `NoMemoryError`: When Ruby was unable to allocate memory for something.
* `ScriptError`: When a script error was encountered.
    * `LoadError`: When a script or module could not be loaded with `require`.
    * `NotImplementedError`: When you call a platform-specific method (`fork` for example) and that method is not available on your platform.
    * `SyntaxError`: When the syntax of a script loaded with `require` or `eval` is invalid.
* `SecurityError`: When you attempt an operation incompatible with the current `$SAFE` level.
* `SignalException`: When Ruby has received a `kill` command.
    * `Interrupt`: When the user uses CTRL+C in the terminal.
* `SystemExit`: When the `exit` method was called.
* `SystemStackError`: When you created a recursive method that was called too many times or you made a bad monkey patch.