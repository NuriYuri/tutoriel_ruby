# Loops
It's a little strange seeing loops first mentioned after we've defined what methods are - it would've made more sense to explain them directly after conditions. I had my reasons for this. Namely, it's fairly rare to use conventional loops in Ruby. Instead, we tend to call methods that take blocks to accomplish these sorts of tasks, which is why I explained blocks right before we got to loops.

## Conventional Loops

There are two types of conventional loops in Ruby:
* `while` executes its code as long as the condition is true.
* `until` is the opposite of `while`. It executes its code as long as the condition is false.

`while` and `until` loops can both be written like conditions, in that they can be put after a condition to create a one-liner.

Examples:
```ruby
i = 0
while i < 5
  i = i + 1
end
# Fera, i = 1, i = 2, i = 3, i = 4, i = 5
i = 0
until i >= 5
  i = i + 1
end
# Fera, i = 1, i = 2, i = 3, i = 4, i = 5
i = 0
# Compact version of the first while:
i += 1 while i < 5
i = 0
# Compact version of the first until:
i += 1 until i >= 5
```

## Making a Do-While
A do-while in Ruby is a little ugly. It looks like this:
```ruby
i = 0
begin
  i += 1
end while i < 5
```

## For Loops
These don't exist in Ruby. It might look like they do, but they're a total lie.

In practically every programming language, a for loop is made up of an initialization, a stop condition, and code executed upon looping, generally designed to get you to the stop condition.

In Ruby, it's a just a variable that we're going to call the `each` method on.

Here's how a for loop is written in Ruby:
```ruby
for variable in object_that_has_each_method
  # execute code
end
```

Example:
```ruby
for i in 0...5
  p i
end
# Prints:
# 0
# 1
# 2
# 3
# 4
```

Instead of `for`, we use explicit methods (which doesn't require creating an iteration object) or otherwise we just directly call the `each` method.

## Loop 0 to n exclusive

This loop is pretty simple. We call the `times` method with whole numbers.

Example:
```ruby
5.times { |i| p i }
# Prints:
# 0
# 1
# 2
# 3
# 4
```

If you want to use this method with an array size, don't. There's a better way:
```ruby
tableau = [1, 2]
tableau.each_index { |i| p i }
# Prints:
# 0
# 1
```

## Loop x to y (inclusive)

This type of loop is written in three different ways:
* `x.upto(y)` if you want to go from x to y (x <= y) in increments of 1.
* `x.downto(y)` if you want to go from x to y (x >= y) in increments of -1.
* `x.step(y, step)` if you want to go from x to y by defining an increment size.

Examples:
```ruby
1.upto(2) { |i| p i }
# Prints:
# 1
# 2
2.downto(1) { |i| p i }
# Prints:
# 2
# 1
4.step(0, -2) { |i| p i }
# Prints:
# 4
# 2
# 0
```

## Flow Control in Loops

You have three keywords you can use in loops to control the execution flow:
* `break` stops the loop. It's possible to give it a parameter, in which case that will become the return value of the loop.

  ```ruby
  value = 5.times do |i|
    p i
    break # generally with a condition
  end
  # Prints:
  # 0
  # value = nil # No parameter given to break
  value = 5.times do |i|
    p i
    break(i)
  end
  # Prints:
  # 0
  # value = 0
  ```

  Note that by default if there's no `break`, the value returned by times is `self` (in this example, 5).

* `next` skips to the next iteration. A parameter to `next` will be returned by `yield` in the `call` method.

  ```ruby
  5.times do |i|
    next if (i % 2).zero?
    p i
  end
  # Prints:
  # 1
  # 3
  my_proc = proc { |i| next(i * 5) }
  my_proc.call(3) # 15
  ```

* `redo` lets you restart the current iteration. (`redo` doesn't take any parameters).

  ```ruby
  a = 0
  5.times do |i|
    if a != i
      puts "a = #{a}"
      a += 1
      redo
    end
    puts "i = #{i}"
    a = 0
  end
  # Prints:
  # i = 0
  # a = 0
  # i = 1
  # a = 0
  # a = 1
  # i = 2
  # a = 0
  # a = 1
  # a = 2
  # i = 3
  # a = 0
  # a = 1
  # a = 2
  # a = 3
  # i = 4
  ```

## Infinite Loops

In Ruby, you can make infinite loops using the `loop` method.

Examples:
```ruby
loop do
  puts "This is the song that doesn't end. Yes it goes on and on my friend! Some people started singing it not knowing what it was, and they'll continue singing it forever just because..." # watch?v=0U2zJOryHKQ
end
# Prints:
# This is the song that doesn't end. Yes it goes on and on my friend! Some people started singing it not knowing what it was, and they'll continue singing it forever just because...
# This is the song that doesn't end. Yes it goes on and on my friend! Some people started singing it not knowing what it was, and they'll continue singing it forever just because...
# This is the song that doesn't end. Yes it goes on and on my friend! Some people started singing it not knowing what it was, and they'll continue singing it forever just because...
# This is the song that doesn't end. Yes it goes on and on my friend! Some people started singing it not knowing what it was, and they'll continue singing it forever just because...
# ...
```

You can accomplish some useful things by using flow control inside an infinite loop.

## Closing Thoughts
We've seen the main types of loops in Ruby, but there are also `Enumerator`-based loops. I would definitely recommend you take a look at these methods (`map`, `select`, `reject` etc...) because they can help you avoid writing unoptimized loops that are difficult to understand.