# Methods and Blocks

Methods are functions - they are used to break up and organize code, helping you to avoid repeating complex tasks over and over throughout your code.

In Ruby, the way methods work is a little more complicated than in most other programming languages: they can have parameterized code blocks. Remember how we iterated over arrays? We just gave a bit of code to the `each` method, and it used that to perform the loop.

## Defining a Method

To define a method in Ruby, we use the `def` keyword, followed by a name in the format of a local variable. If the method takes parameters, you can choose to put them between parentheses (the clearest way), or you can put a space after the name followed by the parameters.

Example: 
```ruby
def zero
  0
end

def addition(a, b)
  return a + b
end
```

These two methods are defined outside of a class or module. This means they are implicitly defined within the current scope. Usually when you do this, it's because you want them in global scope.

If we call `zero`, we'll get back `0`. As you can see, in Ruby the last value of a method is what it returns. Be careful with this, because the "last value" is not always what it might appear to be. This is why we have the `return` keyword, which lets us state explicitly which is the return value. I've done this in the `addition` method.

## Calling a Method

There are several ways to call a method.

### In the same scope where it was defined

In this case, just write the name. For example:
```ruby
addition(6, 12) # 18
```

Note that if you're in another method or class that shares the scope where your method was defined, you can also call it in this same way.

### When you're in a different scope from where the method was defined, but the method's scope is inside an object

You can use the `.` notation here, like we saw with arrays, strings, etc...
```ruby
"The string".addition(6, 12) # 18
```

This works since the method above was defined in global scope, which is owned by `Object`, which every object inherits from (discounting rare exceptions). That's why the method can be accessed through strings, and why it's recommended to avoid defining methods in the way I showed above, but we'll get into that in later chapters.

### When we want to send a message

Ruby uses "messages" to communicate with objects. There are two methods for this: `send`, for public or private methods, and `public_send`, for public methods only.

If you're writing a program with dynamic linking, such as one used for playing animations, you might only have the symbol (method name) and the parameters of your methods. If this is the case you can invoke `send` or `public_send` to call your method.

Example
```ruby
send(:addition, 6, 12) # 18
```

This is more or less what Ruby is doing when it's converting your code into machine language.

## Method Parameters

A method can accept several types of parameters.
* Classic parameters: `local_variable_name`
* Parameters with default values `local_variable_name = value`
* "Splat" parameters: `*args`

    `args` will contain an ordered array containing all the arguments given by the user, onward from the comma after the last non-`args` parameter.
* Named parameters: `local_variable_name:`
* Named parameters with a default value: `local_variable_name: value`

    Named parameters are the equivalent of kwargs in Python, but the difference is the way in which they're written: with a `:` at the end of the name.
* Double "splat" parameters: ``**kwargs`` is the equivalent of the options parameter above that transforms into an array, but the options are explicitly named, making it an associative array. It's mandatory to use this if you want arbitrary named parameters but you've used a named parameter in the function definition already.

Examples with method definitions and calls:
```ruby
def do_something(parameter1, parameter2 = false)
  puts "parameter1 = #{parameter1.inspect}"
  puts "parameter2 = #{parameter2.inspect}"
end

def do_something_else(parameter3, *args)
  puts "parameter3 = #{parameter3.inspect}"
  puts "args = #{args.inspect}"
end

def do_something_named(my_param: , my_optional_param: 0)
  puts "my_param = #{my_param.inspect}"
  puts "my_optional_param = #{my_optional_param.inspect}"
end

def do_something_mixed(parameter4, named_param: nil)
  puts "parameter4 = #{parameter4.inspect}"
  puts "named_param = #{named_param.inspect}"
end

do_something('test')
# Prints:
# parameter1 = "test"
# parameter2 = false
do_something(0, 1)
# Prints:
# parameter1 = 0
# parameter2 = 1
do_something_else(:symbol)
# Prints:
# parameter3 = :symbol
# args = []
do_something_else(0, 1, 2, test: 33)
# Prints:
# parameter3 = 0
# args = [1, 2, {:test=>33}]
do_something_named(my_param: false)
# Prints:
# my_param = false
# my_optional_param = 0
do_something_named(my_param: true, my_optional_param: 'test')
# Prints: 
# my_param = true
# my_optional_param = "test"
do_something_mixed(5)
# Prints:
# parameter4 = 5
# named_param = nil
do_something_mixed(6, named_param: 99)
# Prints:
# parameter4 = 6
# named_param = 99
```

After seeing all these examples, you should be realizing that Ruby gives you a lot of decisions to make when you're defining your methods and parameters.

Generally, we use named parameters for two reasons:
* The method is intended for novice users, and it's worth using parameter names to call out what the parameters are and what to expect them to do.
* The parameter is purely optional, but we want the function to be usable without having to write all the other optional parameters if they only want one (named parameters don't have an order, but unnamed parameters are strictly ordered).

## Defining an Alias
In Ruby it's possible to define aliases. This allows us to provide alternative names or monkey patch things.

Create an alias like this:
```ruby
alias new_name old_name
```

When you create an alias, you're creating an identical copy of the method you're aliasing. This aliased method can then be redefined and you can still call its alias to get the old behavior (and do monkey patching).

Example:
```ruby
def my_method(param)
  param + 5
end
alias my_old_method my_method

def my_method(param)
  my_old_method(param.to_i)
end

my_method("5") # 25
```

Without this little monkey patch, the method would've crashed the program when called with that parameter. Of course keep in mind this isn't typically the kind of thing we intend to fix when we're doing a monkey patch.

## Blocks

Blocks are a special kind of parameter. So special, in fact, that they need their own dedicated section.
Like I said earlier, blocks are bits of code that we give to a function so it can accomplish some task (like `each`).

If you want to use blocks, you have two ways to do it:
* Using the keyword `yield` (make sure you actually got a block by calling `block_given?`)
* Specifying a parameter starting with the `&` symbol to tell Ruby you want a block.

The parameter starting with `&` doesn't have to be used, except for the two following reasons:
* You're not going to execute the block, intending instead to save it and use it later.
* You want to execute the block in some specialized way (using `instance_eval`, `instance_exec` or other Ruby methods that are used for special operations).

Important notes:

* If no block is given to a parameter starting with `&`, the local named variable after the `&` will be `nil`.
* The parameters given to `yield` will be the parameters passed to the block.
* Blocks accept parameters like methods, but these parameters need to be separated with `|` after the opening curly brace (for single-line blocks) or  the `do` (for multiline blocks).

Examples:
```ruby
def execute_yield_block
  if block_given?
    yield(5)
  else
    puts "You didn't pass in a block..."
  end
end

def execute_block(&block)
  if block
    block.call(5)
  else
    puts "You didn't pass in a block..."
  end
end

execute_yield_block { |i| i * 5 } # 25
execute_block { |i| i * 5 } # 25
execute_yield_block do |variable|
  multiplication = variable * 6
  puts "result = #{multiplication}"
end
# Prints:
# result = 30
execute_block do |variable|
  multiplication = variable * 6
  puts "result = #{multiplication}"
end
# Prints:
# result = 30
```

## Special Block Method Calls

As you've seen, you use the `&block_name` syntax in a method's parameters. This syntax can also be used when calling the method to pass in objects as a block or have `yield` call a method in place of a block. 

### Sending a Proc or a Method as a block

We haven't seen this yet, but there are special objects that respond to the `call` method. They have the type `Proc` and `Method`.

A `Proc` is a bit of Ruby code defined with a block. What's special about `Proc`s is that they save the scope of the code around it upon being created. Be careful here: the same scope is conserved even if you create another `Proc` within the first `Proc`.

A `Method` is an object that points to an object method. We can save this pointer and use it to call that method with `call` later on.

Examples
```ruby
times_five_in_proc = Proc.new { |i| 5 * i }
times_five = 5.method(:*) # This is how we create a Method object
times_five_in_proc.call(5) # 25
times_five.call(6) # 30
```

Notes:
* `Proc.new` can be replaced by `proc`

    ```ruby
    times_five_in_proc = proc { |i| 5 * i }
    ```
* We can directly write `five_times.(6)` instead of `five_times.call(6)`


Now that we've seen how to create `Proc` and `Method` objects, we can see how to send them to a method that accepts a block.

The `collect` method of the `Array` class takes a block that it uses to transform an element into an array. We're going to call it with a `Proc` and a `Method`:
```ruby
array = [1, 2, 3]
times_five_in_proc = Proc.new { |i| 5 * i }
times_five = 5.method(:*)
array.collect(&times_five_in_proc) # [5, 10, 15]
array.collect(&times_five) # [5, 10, 15]
```

### Calling a method when its first parameter is intended to be passed to a block

It's also possible to call a method when its first parameter is passed, or intended to be passed, to a block. This lets us reduce the size of our code and can be more efficient. To do this, we use the syntax `&:method_name`. The difference here is that we put a `:` between the `&` and the method name.

Example
```ruby
array = [1, 2, 3]
array.collect(&:to_s) # ["1", "2", "3"]
```

Of course, the method doesn't necessarily have to have parameters (which is only the case for iteration methods like `Enumerable`). You can guarantee this when you're making your own functions. The method called by `yield` in this case will be what's passed in, and its arguments will be yield's arguments.

Example:
```ruby
def find_my_two_numbers
  yield(5, 6)
end
find_my_two_numbers(&:+) # 11
find_my_two_numbers(&:*) # 30
find_my_two_numbers(&:-) # -1
```

## Closing Thoughts

That about wraps up defining methods and using blocks. In my opinion, Ruby's blocks are a very powerful system that you should use to simplify life when you're coding.