# Ruby Programming Tutorial
## Introduction

This tutorial is intended to teach the basics of Ruby programming on modern versions of Ruby (2.5+). There are some other very well-made tutorials on this topic ([French video tutorial by 
Grafikart.fr](https://www.youtube.com/watch?v=PmVcZK3Jwy4&t=0s&list=PLjwdMgw5TTLVVJHvstDYgqTCao-e-BgA8&index=4)), and there are some whose approach I disagree with.

This tutorial will take the approach of explanations via text, accompanied by numerous example scripts.

Keep in mind that this tutorial is not comprehensive - some things you'll want to do can be done with methods that will not appear in this tutorial. I suggest you search on the [Ruby Docs](https://ruby-doc.org/) or just read through documentation pages for individual classes. You should find both to be very useful.

## Table of Contents

1. [Variables and Data Types](1_Variables_and_Data_Types.md)
2. [Ordered Arrays](2_Ordered_Arrays.md)
3. [Associative Arrays](3_Associative_Arrays.md)
4. [Character Strings and Regular Expressions](4_Strings_and_Regexp.md)
5. [Conditionals](5_Conditionals.md)
6. [Methods and Blocks](6_Methods_and_Blocks.md)
7. [Loops](7_Loops.md)
8. [Classes and Objects](8_Classes_and_Objects.md)
9. [Modules](9_Modules.md)
10. [Error Handling](10_Error_Handling.md)
11. [Input/Output](11_IO.md)
12. [Safe Navigation](12_Safe_Navigation.md)