# Class describing a 2D point
class Point2D
  # @return [Integer] x coordinate of the point
  attr_accessor :x
  # @return [Integer] y coordinate of the point
  attr_accessor :y

  # @overload new(x_pos, y_pos)
  #   Create a new point by giving its coordinates
  #   @param x_pos [Integer] x coordinate of the point
  #   @param y_pos [Integer] y coordinate of the point
  # @overload new(point)
  #   Create a new point by copying another point
  #   @param point [Point2D] the other point
  def initialize(x_pos, y_pos = 0)
    set_coordinates(x_pos, y_pos)
  end

  # Convert an input value to a Integer
  # @param value [Numeric, String]
  # @return [Integer] 
  private def safe_to_i(value)
    return value.to_i if value.is_a?(Numeric) || value.is_a?(String)
    return value.to_s.to_i
  end

  # Define the new x coordinate of the point
  # @param value [Integer] new x coordinate
  def x=(value)
    @x = safe_to_i(value)
  end

  # Define the new y coordinate of the point
  # @param value [Integer] new y coordinate
  def y=(value)
    @y = safe_to_i(value)
  end

  # @overload set_coordinates(x_pos, y_pos)
  #   Set the new coordinate of the point
  #   @param x_pos [Integer] x coordinate of the point
  #   @param y_pos [Integer] y coordinate of the point
  #   @return [self]
  # @overload set_coordinates(point)
  #   Set the new coordinate of the point by copying another point
  #   @param point [Point2D] the other point
  #   @return [self]
  def set_coordinates(x_pos, y_pos = 0)
    return set_coordinates(x_pos.x, x_pos.y) if x_pos.is_a?(Point2D)
    self.x = x_pos
    self.y = y_pos
    return self
  end

  # @overload move(delta_x, delta_y)
  #   Move the point by a certain amount of delta_x and a certain amount of delta_y
  #   @param delta_x [Integer] amount of x we want to move the point
  #   @param delta_y [Integer] amout of y we want to move the point
  #   @return [self]
  # @overload move(delta_point)
  #   Move the point by a certain amount of delta_point coordinates
  #   @param delta_point [Point2D]
  #   @return [self]
  def move(delta_x, delta_y = 0)
    return move(delta_x.x, delta_x.y) if delta_x.is_a?(Point2D)
    @x += safe_to_i(delta_x)
    @y += safe_to_i(delta_y)
    return self
  end

  # Calculate the square distance of the point to another point
  # @param other_point [Point2D] the other point we want to calculate the square distance
  # @return [Integer] the square distance
  private def distance2(other_point)
    return (@x - other_point.x)**2 + (@y - other_point.y)**2
  end

  # Calculate the distance of the point to another point
  # @param other_point [Point2D]
  # @return [Integer] the distance
  def distance(other_point)
    return (distance2(other_point)**0.5).round
  end
end