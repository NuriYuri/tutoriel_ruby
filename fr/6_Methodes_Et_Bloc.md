# Les méthodes et les blocs

Les méthodes sont des fonctions, elles ont pour objectif de factoriser le code et d'éviter de répéter des tâches complexes tout au long du code.

En Ruby, les méthodes ont un fonctionnement un peu plus avancé que dans la plupart des langages de programmation : elles peuvent avoir des blocs de code en paramètre. Vous vous rappelez l'exploration dans les tableaux ? On donnait justement une portion de code à la méthode `each` pour réaliser notre opération d'exploration.

## Définir une méthode

En Ruby, pour définir une méthode, on écrit le mot clé `def` qu'on suit d'un nom correspondant à une variable locale. Si la méthode prend des paramètres, on peut soit indiquer les paramètres entre parenthèses (le plus clair) soit mettre un espace après le nom et donner les paramètres.

Exemple : 
```ruby
def zero
  0
end

def addition(a, b)
  return a + b
end
```

Ces deux méthodes sont définies en dehors d'une classe ou d'un module, de ce fait, elles seront définies dans le contexte courant (Object bien souvent : contexte global).

Si on appelle `zero` nous obtiendrons `0`. En effet, en Ruby la dernière valeur est ce qui est renvoyée par la méthode. Attention, la dernière valeur n'est pas toujours ce que l'on croit, c'est pourquoi il y a un mot clé `return` qui permet de dire explicitement quel est le retour. Ce que j'ai fait dans la méthode `addition`.

## Appeler une méthode

Il existe plusieurs manières d'appeler une méthode.

### Quand on est dans le contexte de sa définition

Il suffit d'écrire son nom, par exemple :
```ruby
addition(6, 12) # 18
```

Notez que si vous êtes dans une méthode qui est du même contexte ou qui est dans une classe héritée vous pouvez également utiliser cette méthode.

### Quand vous n'êtes pas dans le contexte de sa définition mais qu'un objet possède cette méthode

Vous utiliserez la notation `.` comme on a vu dans les tableaux, chaînes etc...
```ruby
"Une chaîne".addition(6, 12) # 18
```

Notez que ici ça fonctionne car plus haut la méthode a été définie dans un contexte global (`Object`), tous les objets (à part rares exceptions) héritent de `Object`. De ce fait les chaînes ont cette méthode. (C'est pourquoi il est recommandé d'éviter de définir des méthodes comme je vous ai montré, mais ça on verra dans les chapitres suivants.)

### Quand on veut envoyer un message

Ruby utilise des "messages" pour communiquer avec les objets. Il y a deux méthodes `public_send` (pour les méthodes publiques) et `send` pour les méthodes publiques ou privées.

Si vous écrivez un programme dynamique comme un afficheur d'animation, il se peut que vous ne stockiez que le symbole et les paramètres de vos méthodes. Pour cela pas de soucis, vous pouvez invoquer `send` ou `public_send` pour appeler votre méthode.

Exemple :
```ruby
send(:addition, 6, 12) # 18
```

Ce mécanisme est plus ou moins ce que Ruby fait quand il a converti votre code en instructions élémentaires.

## Les paramètres d'une méthode

Une méthode peut accepter plusieurs types de paramètres.
* Les paramètres classiques : `nom_variable_locale`
* Les paramètres ayant des valeurs par défaut : `nom_variable_locale = valeur`
* Les paramètres dits "splat" : `*args`

    `args` contiendra un tableau ordonné contenant tous les arguments donnés par l'utilisateur à partir de la virgule de cet argument.
    Le dernier argument peut potentiellement être un tableau associatif selon la manière dont la méthode a été appelée.
* Les paramètres nommés : `nom_variable_local:`
* Les paramètres nommés à valeur par défaut : `nom_variable_local: valeur`

    Les paramètres nommés sont l'équivalent des kwargs de Python mais la différence est la manière de les écrire, ils s'écrivent avec un `:` à la fin du nom.
* Les paramètres double "splat" : ``**kwargs`` c'est l'équivalent du paramètre d'option (le dernier paramètre qui se transforme en tableau associatif) mais explicite. C'est obligatoire si vous voulez obtenir plus de paramètres nommés lorsque vous en avez déjà utilisé un dans la définition.

Exemples de définition et d'appel :
```ruby
def faire_quelque_chose(parametre1, parametre2 = false)
  puts "parametre1 = #{parametre1.inspect}"
  puts "parametre2 = #{parametre2.inspect}"
end

def faire_autre_chose(parametre3, *args)
  puts "parametre3 = #{parametre3.inspect}"
  puts "args = #{args.inspect}"
end

def faire_une_chose_nomme(mon_param: , mon_param_optionnel: 0)
  puts "mon_param = #{mon_param.inspect}"
  puts "mon_param_optionnel = #{mon_param_optionnel.inspect}"
end

def faire_qqch_melange(parametre4, param_nomme: nil)
  puts "parametre4 = #{parametre4.inspect}"
  puts "param_nomme = #{param_nomme.inspect}"
end

faire_quelque_chose('test')
# Affiche :
# parametre1 = "test"
# parametre2 = false
faire_quelque_chose(0, 1)
# Affiche :
# parametre1 = 0
# parametre2 = 1
faire_autre_chose(:symbol)
# Affiche :
# parametre3 = :symbol
# args = []
faire_autre_chose(0, 1, 2, test: 33)
# Affiche :
# parametre3 = 0
# args = [1, 2, {:test=>33}]
faire_une_chose_nomme(mon_param: false)
# Affiche :
# mon_param = false
# mon_param_optionnel = 0
faire_une_chose_nomme(mon_param: true, mon_param_optionnel: 'test')
# Affiche : 
# mon_param = true
# mon_param_optionnel = "test"
faire_qqch_melange(5)
# Affiche :
# parametre4 = 5
# param_nomme = nil
faire_qqch_melange(6, param_nomme: 99)
# Affiche :
# parametre4 = 6
# param_nomme = 99
```

Avec ceci vous pouvez constater que Ruby offre des options assez intéressantes concernant les paramètres de fonction.

Généralement nous utiliserons les paramètres nommés pour deux raisons :
* La méthode est destinée à des utilisateurs novices, c'est plus intéressant d'afficher le nom des paramètres dans l'appel ainsi ils savent ce qu'ils affectent.
* Le paramètre est purement optionnel mais nous désirons qu'il puisse être utilisé sans que l'utilisateur ait à écrire tous les autres paramètres optionnels qui arrivent avant (car les paramètres nommés n'ont pas d'ordre).

## Définir un alias
En Ruby il est possible de définir des alias, ceci permet de donner des noms alternatifs ou de réaliser du monkey patch.

L'alias s'utilise ainsi :
```ruby
alias nouveau_nom ancien_nom
```

Quand vous créez un alias, vous créez en fait une copie identique de la méthode que vous aliassez au moment de l'alias. De ce fait quand vous redéfinissez la méthode aliassé vous pouvez appeler son alias pour exécuter l'ancien comportement (et faire du monkey patch).

Exemple : 
```ruby
def ma_methode(param)
  param + 5
end
alias mon_ancienne_method ma_method

def ma_methode(param)
  mon_ancienne_methode(param.to_i)
end

ma_methode("5") # 25
```

Sans ce petit monkey patch, la méthode aurait provoqué un crash du logiciel avec un paramètre pareil. Bien entendu c'est pas vraiment ainsi que l'on se sert du monkey patch le plus souvent.

## Les blocs

Les blocs sont des paramètres un peu spéciaux, du coup ils ont besoin d'une partie bien à eux. 
Comme je l'ai dit plus tôt, les blocs sont des morceaux de code que l'on donne à une fonction pour qu'elle exécute une certaine tâche (comme `each`).

Si vous voulez utiliser le mécanisme des blocs vous avez deux manières de le faire :
* En utilisant le mot clé `yield` (en vérifiant que le bloc a bien été donnée avec `block_given?`).
* En spécifiant un paramètre commençant par le symbole `&` pour dire à Ruby que vous voulez un bloc.

Le paramètre commençant par `&` ne doit être utilisé que pour les deux raisons suivantes :
* Vous n'allez pas exécuter le bloc mais plutôt le sauvegarder dans l'objectif de le sauvegarder plus tard.
* Vous désirez exécuter le bloc d'une manière un peu spécifique (à l'aide de `instance_eval`, `instance_exec` ou d'autres méthodes de Ruby pour réaliser des opérations spécifiques).

Notes particulières :

* Avec le paramètre commençant par `&` si aucun bloc n'est donné, la variable local nommée juste après le `&` vaut `nil`.
* Les paramètres donnés à `yield` sont les paramètres envoyés au bloc.
* Les blocs acceptent des paramètres comme les méthodes mais ces paramètres doivent être spécifiés entre `|` après l'accolade d'ouverture (bloc en une ligne) ou le `do` (bloc en plusieurs lignes).

Exemples :
```ruby
def executer_bloc_yield
  if block_given?
    yield(5)
  else
    puts "Vous n'avez pas donné de bloc..."
  end
end

def executer_bloc(&block)
  if block
    block.call(5)
  else
    puts "Vous n'avez pas donné de bloc..."
  end
end

executer_bloc_yield { |i| i * 5 } # 25
executer_bloc { |i| i * 5 } # 25
executer_bloc_yield do |variable|
  multiplication = variable * 6
  puts "resultat = #{multiplication}"
end
# Affiche :
# resultat = 30
executer_bloc do |variable|
  multiplication = variable * 6
  puts "resultat = #{multiplication}"
end
# Affiche :
# resultat = 30
```

## Appels spéciaux de méthodes à bloc

Comme vous avez pu le voir, il y a la syntaxe `&nom_bloc` pour les paramètres de la méthode. Cette syntaxe peut également être exploitée lors de l'appel pour envoyer des objets en tant que bloc ou appeler une méthode du premier paramètre de `yield` à la place d'un bloc.

### Envoyer un Proc ou une Method en tant que bloc

Nous ne l'avons pas encore vu mais il existe des objets répondant à la méthode `call`. Ce sont les `Proc` et les `Method`.

Un `Proc` est un morceau de code Ruby défini à l'aide d'un bloc, la particularité des `Proc` c'est qu'ils enregistrent le contexte du code lors de sa création. (Attention, le contexte est conservé, même si vous recréez un `Proc` à partir de ce `Proc`).

Une `Method` est un objet qui pointe vers une méthode d'un objet. On peut appeler la méthode à l'aide de `call` plus tard.

Exemples : 
```ruby
cinq_fois_en_proc = Proc.new { |i| 5 * i }
cinq_fois = 5.method(:*) # C'est ainsi que l'on crée un objet Method
cinq_fois_en_proc.call(5) # 25
cinq_fois.call(6) # 30
```

Notes :
* `Proc.new` peut être remplacé par `proc`

    ```ruby
    cinq_fois_en_proc = proc { |i| 5 * i }
    ```
* On peut directement écrire `cinq_fois.(6)` au lieu de `cinq_fois.call(6)`


Maintenant qu'on a vu comment créer des `Proc` ou des `Method` on peut voir comment les envoyer à une méthode qui accepte un bloc.

La méthode `collect` de la classe Array accepte un bloc qui sert à transformer un élément du tableau, nous allons l'appeler avec un `Proc` et une `Method` :
```ruby
array = [1, 2, 3]
cinq_fois_en_proc = Proc.new { |i| 5 * i }
cinq_fois = 5.method(:*)
array.collect(&cinq_fois_en_proc) # [5, 10, 15]
array.collect(&cinq_fois) # [5, 10, 15]
```

### Appeler une méthode du premier paramètre censé être passé au bloc

Il est également possible d'appeler une méthode du premier paramètre passé ou qui est censé être passé au bloc, ceci permet de réduire la taille du code et d'être plus efficace. Pour ce faire, on utilise la syntaxe `&:nom_methode`. La différence ici c'est qu'on a mis un `:` entre le `&` et le nom de la méthode.

Exemple :
```ruby
array = [1, 2, 3]
array.collect(&:to_s) # ["1", "2", "3"]
```

Bien entendu, il n'est pas obligatoire que la méthode utilisée n'ait pas de paramètre (c'est seulement le cas pour les méthodes d'itération des `Enumerable`). Si vous faites vos propres fonctions, les paramètres qui viennent après l'objet dans `yield` sont les paramètres envoyés à la méthode.

Exemple : 
```ruby
def trouve_mes_deux_nombres
  yield(5, 6)
end
trouve_mes_deux_nombres(&:+) # 11
trouve_mes_deux_nombres(&:*) # 30
trouve_mes_deux_nombres(&:-) # -1
```

## Mot de la fin

Voilà pour la définition des méthodes et de l'usage des blocs. De mon point de vue, les blocs en Ruby sont un système ultra puissant qui permet de se simplifier la vie.